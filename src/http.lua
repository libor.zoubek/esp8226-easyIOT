-- https://github.com/mlk/nodemcu-http-client/blob/master/http.lua
-- lzoubek: fix components.port regex
local http = {}

function http.parseUrl(url)
    local components = {}
    components.scheme = string.match(url, "([^:]*):")
    components.host = string.match(url, components.scheme .. "://([^:/]*)[:/]?")
    components.port = string.match(url, components.scheme.."://[^:]*:([%d]*)")
    baseUrl = components.scheme .. "://" .. components.host
    if components.port ~= nil then
        baseUrl = baseUrl .. ":" .. components.port
    end
    components.pathAndQueryString = string.sub(url, string.len(baseUrl) + 1)
    return components
end

function http.getContent(url, callWithData)
    http.sendContent("GET", url, nil, nil, nil, callWithData)
end

function http.postContent(url, content, contentType, header, callWithData)
    http.sendContent("POST", url, content, contentType, header, callWithData)
end

function http.sendContent(method, url, contentToSend, contentType, header, callWithData)
    local components = http.parseUrl(url)
    if components.port == nil then
        components.port = 80
    else
        components.port = tonumber(components.port)
    end
    if components.pathAndQueryString == nil or components.pathAndQueryString == "" then
        components.pathAndQueryString = "/"
    end

    if contentType == nil then
        contentType = "application/x-www-form-urlencoded"
    end
    
    local conn=net.createConnection(net.TCP, 0)

    conn:on("connection", function(conn) 
        conn:send(method .. " " .. components.pathAndQueryString .. " HTTP/1.0\r\nHost: " .. components.host .. "\r\n"
            .. "Accept: */*\r\n")
            
        if header ~= nil then
            conn:send(header .. "\r\n");       
        end
        if contentToSend ~= nil then
            conn:send("Content-Type: " .. contentType .. "\r\n");
            conn:send("Content-Length: " .. string.len(contentToSend) .. "\r\n\r\n")
            conn:send(contentToSend)
        else
            conn:send("\r\n")
        end
        
    end)
    conn:on("receive", function(conn, pl)        
        local data = {}
        data.status = string.match(pl, "HTTP/%d.%d (%d+)")
        local location = string.find(pl, "\r\n\r\n")        
        if location ~= nil then
            data.content = string.sub(pl, location + 4)
        end
        pl = nil
        collectgarbage()
        conn:close()
        conn = nil
        
        callWithData(data)       

    end)
    conn:connect(components.port, components.host)
end

return http
