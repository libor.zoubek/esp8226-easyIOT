local ssid = "ssid"
local wifiPass = ""

-- EesyIOT acces token
local token = "..."

-- how often do we measure? (millis)
local interval = 120 * 1000

-- sensor
-- gpio is sensor index (from the board)
-- id is EasyIOT module Parameter ID
local sensor = {gpio = 4, id = 'parameter-id'}-- udirna

-- connect to wifi 
wifi.setmode(3);
wifi.setmode(wifi.STATION);
wifi.sta.config(ssid,wifiPass);
wifi.sta.connect();
wifi.sta.autoconnect(1);
print(wifi.sta.getip());
-- start measuring and pushing data
local sender = require("sender")(interval, sensor, token)
print("Waiting 7 seconds before measuring")
tmr.alarm(0, 7 * 1000, 1, function()
 sender.start() 
 tmr.stop(0)
end)
