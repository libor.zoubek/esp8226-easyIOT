
--------------------------------------------------------------------------------
---- EasyIOT Cloud feed
---- LICENCE: http://opensource.org/licenses/MIT
---- lzoubek <lzoubek@jezzovo.net>
----------------------------------------------------------------------------------
local sender =  {}

function round(num, idp)
    local mult = 10^(idp or 0)
    if num >= 0 then return math.floor(num * mult + 0.5) / mult
    else return math.ceil(num * mult - 0.5) / mult end
end

function read_temp(gpio)
  local t = require("ds18b20")
  t.setup(gpio)
  local temp = t.read()
  if temp == nil or temp == 85 then
    return nil
  end
  temp = round(temp, 1)
  print("Temperature: "..temp.." 'C")
  t = nil
  ds18b20 = nil
  package.loaded["ds18b20"] = nil
  return temp
end


function post_data(instanceId, parameterId, value)

    local url = 'http://cloud.iot-playground.com:40404/RestApi/v1.0/Parameter/'..parameterId..'/Value/'..tostring(value)
    local http = require("http")
    print("POST: "..url)
    http.postContent(url, "", "application/json", "EIOT-AuthToken: " .. sender.token, function(data)
        print("Response "..tostring(data.status))
        --print("Going to deepsleep for "..(sender.sleep/1000).."s")
        --node.dsleep(sender.sleep * 1000)
    end)
end

function sender.start()
    print("Measuring...")
            local temp = read_temp(sender.sensor['gpio'])
            if (temp == nil) then
                tmr.delay(5 * 1000 * 1000)
                temp = read_temp(sender.sensor['gpio'])
            end
            if not (temp == nil) then
                print("Sending data")
                post_data(sender.instanceId, sender.sensor['id'], temp)
            end
       --print("Waiting 20s to autosleep")
       --tmr.alarm(1, 20 * 1000, 1, function()
        --print("Timed out");
        --tmr.stop(1)
        --print("Going to deepsleep for "..(sender.sleep/1000).."s")
        --node.dsleep(sender.sleep * 1000)
       --end)
end
return function(sleep, sensor, token)
    sender.sleep = sleep
    sender.sensor = sensor
    sender.token = token
    return sender
end
